#include <stdio.h>
#include <SDL2/SDL.h>
#include <vector>

const int SCREEN_WIDTH=960;
const int SCREEN_HEIGHT=960;
const int PIXEL_WIDTH = SCREEN_WIDTH/64;
const int PIXEL_HEIGHT = SCREEN_HEIGHT/64;

void activateCell(bool** cells, int x, int y) {
    int x_grid = x / PIXEL_WIDTH;
    int y_grid = y / PIXEL_HEIGHT;
    cells[y_grid][x_grid] = true;
}

int getNumNeighbours(bool** cells, int x, int y) {
    int num = 0;
    for (int ny = y-1; ny < y+2; ny++) {
        for (int nx = x-1; nx < x+2; nx++) {
            if (nx < 0 || ny < 0 || nx > 63 || ny > 63) {
                break;
            }else if(nx == x && ny == y) {
                continue;
            }else if(cells[ny][nx]) {
                num++;
            }
        }
    }
    return num;
}

void simulateCycle(bool** cells) {
    std::vector<int> tolive;
    std::vector<int> todie;
    for (int y = 0; y < 64; y++) {
        for (int x = 0; x < 64; x++) {
            int numNeighbours = getNumNeighbours(cells, x, y);
            if (numNeighbours < 2 || numNeighbours >= 4) {
                todie.push_back(y*64+x);
            }else if(numNeighbours == 3) {
                tolive.push_back(y*64+x);
            }
        }
    }
    while(tolive.size() > 0) {
        int num = tolive.back();
        tolive.pop_back();
        int x = num % 64;
        int y = num / 64;
        cells[y][x] = true;
    }
    while(todie.size() > 0) {
        int num = todie.back();
        todie.pop_back();
        int x = num % 64;
        int y = num / 64;
        cells[y][x] = false;
    }
}

int main(int argc, char** argv) {
    printf("Hello there.\n");

    SDL_Window* window = nullptr;
    SDL_Renderer* renderer = nullptr;

    bool** cells = new bool*[64];
    for (int i=0; i<64; i++) {
        cells[i] = new bool[64];
        for (int j=0;j<64;j++) {
            cells[i][j] = false;
        }
    }

    bool quit = false;
    bool simulate = false;

    //Init SDL
    if(SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
        quit = true;
    }else {
        window = SDL_CreateWindow("SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
        if (window == nullptr) {
            printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
            quit = true;
        }else {
            renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
            if (renderer == nullptr) {
                printf("Renderer could not be created! SDL_Error: %s\n", SDL_GetError());
                quit = true;
            }
        }
    }

    SDL_Event e;
    while(!quit) {
        while(SDL_PollEvent(&e) != 0) {
            switch(e.type) {
                case SDL_QUIT:
                quit=true;
                break;
                case SDL_MOUSEBUTTONDOWN:
                if(!simulate) {
                    int x, y;
                    SDL_GetMouseState(&x, &y);
                    activateCell(cells, x, y);
                }
                break;
                case SDL_KEYDOWN:
                switch(e.key.keysym.sym) {
                    case SDLK_RETURN:
                    simulate = true;
                    break;
                }
                break;
            }
        }
        if (simulate) {
            simulateCycle(cells);
            simulate = false;
        }

        
        
        for(int y = 0; y < 64; y++) {
            for (int x = 0; x < 64; x++) {
                if (cells[y][x]) {
                    SDL_SetRenderDrawColor(renderer, 0xFF,0xFF,0xFF,0xFF);
                }else {
                    SDL_SetRenderDrawColor(renderer, 0x00,0x00,0x00,0xFF);
                }
                SDL_Rect fillRect = {x * PIXEL_WIDTH, y * PIXEL_HEIGHT, PIXEL_WIDTH, PIXEL_HEIGHT};
                SDL_RenderFillRect(renderer, &fillRect);
                SDL_SetRenderDrawColor(renderer, 0x00,0x44,0x00,0xFF);
                SDL_RenderDrawRect(renderer, &fillRect);
            }
        }
        SDL_RenderPresent(renderer);
    }
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}